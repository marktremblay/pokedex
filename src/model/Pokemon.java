package model;

public class Pokemon {

    private int numero;
    private String nome;
    private String tipo;
    private String categoria;
    private double altura;
    private double peso;
    private int geracao;

    public Pokemon() {
    }

    public Pokemon(int numero, String nome, String tipo, String categoria,
                   double altura, double peso, int geracao) {
        this.numero = numero;
        this.nome = nome;
        this.tipo = tipo;
        this.categoria = categoria;
        this.altura = altura;
        this.peso = peso;
        this.geracao = geracao;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getGeracao() {
        return geracao;
    }

    public void setGeracao(int geracao) {
        this.geracao = geracao;
    }

    @Override
    public String toString() {
        return String.format("Número: %-5d Nome: %-15s Tipo: %-10s " +
                        "Categoria: %-15s Altura: %-10.1f Peso: %-10.1f Geração: %-10d",
                numero, nome, tipo, categoria, altura, peso, geracao);
    }
}