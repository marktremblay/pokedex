package controller;

import model.Pokemon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Pokedex {

    private static String caminho = "D:\\Documentos\\Java\\Curso Java\\Projeto\\src\\course\\Converter\\model\\Pokedex.csv";
    public static List<Pokemon> lista = new ArrayList<>();


    public static void carregarLista() throws Exception {

        BufferedReader lerArquivo = new BufferedReader(new FileReader(caminho));
        String linha = lerArquivo.readLine();
        lista.clear();

        while(linha != null) {

            String[] vetor = linha.split(",");

            Pokemon pokemon = new Pokemon(
                    Integer.parseInt(vetor[0]),
                    vetor[1],
                    vetor[2],
                    vetor[3],
                    Double.parseDouble(vetor[4]),
                    Double.parseDouble(vetor[5]),
                    Integer.parseInt(vetor[6])
            );

            lista.add(pokemon);
            linha = lerArquivo.readLine();
        }

        lerArquivo.close();
    }

    public static void salvarLista() throws Exception {

        BufferedWriter escreverArquivo = new BufferedWriter(new FileWriter(caminho));

        for(Pokemon pokemon : lista) {

            escreverArquivo.write(lista.indexOf(pokemon) + 1 + "," +
                    pokemon.getNome() + "," +
                    pokemon.getTipo() + "," +
                    pokemon.getCategoria() + "," +
                    pokemon.getAltura() + "," +
                    pokemon.getPeso() + "," +
                    pokemon.getGeracao());
            escreverArquivo.newLine();
        }

        escreverArquivo.close();
    }

    public static void atualizarLista() throws Exception {

        carregarLista();
        salvarLista();
    }

    public static void mostrarLista() {

        System.out.println("\nMostrando todos os Pokémons:");

        for(Pokemon pokemon : lista) {
            System.out.println(pokemon.toString());
        }
    }
}
